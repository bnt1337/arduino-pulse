
const int readpin = 1;
const int pulsepin = 2;

short int i = 0;
const short int pulseNtimes = 200;

void setup( void ) {
  pinMode(readpin, INPUT);
  pinMode(pulsepin, OUTPUT);
  Serial.begin(9600);
}

void loop( void ){
	if ( digitalRead(readpin) == HIGH ){
		for ( i = 0; i < (int)pulseNtimes; i++){
			digitalWrite(pulsepin, HIGH);
			delay(1);
			digitalWrite(pulsepin, LOW);
			delay(1);
		}
	}
}
